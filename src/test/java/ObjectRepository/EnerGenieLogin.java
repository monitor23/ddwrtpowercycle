package ObjectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EnerGenieLogin {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public EnerGenieLogin(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 70);
		PageFactory.initElements(driver, this);
	}

	@FindBy(css="#lForm > div:nth-child(3) > input:nth-child(1)")
	WebElement password;
	@FindBy(css = "#lForm > div:nth-child(3) > input:nth-child(2)")
	WebElement enter;
	

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(password));

	}

	public WebElement Password() {
		return password;
	}

	public WebElement Enter() {
		return enter;
	}

}
