package ObjectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LEDELogin {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public LEDELogin(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 70);
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "div.cbi-value:nth-child(2) > div:nth-child(2) > input:nth-child(1)")
	WebElement password;
	@FindBy(css = "input.btn:nth-child(1)")
	WebElement login;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(password));

	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() {
		return login;
	}

}
