package ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EnerGenieHome {
	WebDriver driver;

	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public EnerGenieHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 180);
		PageFactory.initElements(driver, this);
	}

	public void SelectSocketB() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("div.boxmenuitem:nth-child(7) > a:nth-child(1)"))).click();
	}

	public void SwitchOFFSocketB() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sState")));
		String state = driver.findElement(By.id("sState")).getText();
		if (state.equals("OFF")) {
			System.out.println("Status of Socket B :" + state);
		} else {
			System.out.println("Switching OFF Socket B");
			driver.findElement(By.id("sbtn")).click();
			String uState = driver.findElement(By.id("sState")).getText();
			System.out.println("Updated Status of Socket B :" + uState);
		}
	}

	public void SwitchONSocketB() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sState")));
		String state = driver.findElement(By.id("sState")).getText();
		if (state.equals("ON")) {
			System.out.println("Status of Socket B : " + state);
		} else {
			System.out.println("Switching ON Socket B");
			driver.findElement(By.id("sbtn")).click();
			String uState = driver.findElement(By.id("sState")).getText();
			System.out.println("Updated Status of Socket B :" + uState);
		}
	}

	public void Logout() throws InterruptedException {

		Thread.sleep(5000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("div.boxmenuitem:nth-child(13) > a:nth-child(1)"))).click();
		Thread.sleep(1000);
		driver.close();

	}

}
