package ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LEDEHome {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public LEDEHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 70);
		PageFactory.initElements(driver, this);
	}

	public void OpenWirelessNetworkOverview() throws InterruptedException {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("li.dropdown:nth-child(3) > a:nth-child(1")));
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.cssSelector("li.dropdown:nth-child(3) > a:nth-child(1)"))).build()
				.perform();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("li.dropdown:nth-child(3) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)")))
				.click();
	}

	public void EnableDisableWirelessNetwork() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("#cbi-wireless-wifinet1 > div:nth-child(3) > div:nth-child(1) > button:nth-child(1)")))
				.click();

	}

	public void Logout() throws InterruptedException {
		Thread.sleep(20000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#topmenu > li:nth-child(5) > a:nth-child(1)"))).click();
		Thread.sleep(1000);
		driver.quit();

	}

}
