package TestCase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepository.EnerGenieHome;
import ObjectRepository.EnerGenieLogin;

public class SocketBStatusOFF {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://192.168.5.48");

	}

	@Test
	public void EnerGenieLANPowerOFF() throws InterruptedException {
	
		EnerGenieLogin egl = new EnerGenieLogin(driver);
		egl.WaitFunction();
		egl.Password().sendKeys("adsl55");
		egl.Enter().click();
		EnerGenieHome egh = new EnerGenieHome(driver);		
		egh.SelectSocketB();
		egh.SwitchOFFSocketB();
		egh.SwitchONSocketB();
		egh.Logout();
		
	}
	
	

	
}
