package TestCase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepository.LEDEHome;
import ObjectRepository.LEDELogin;

public class VerifySocketStatus {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://192.168.5.65/cgi-bin/luci/");

	}

	@Test
	public void VerifySocketBStatusON() throws InterruptedException {

		LEDELogin ll = new LEDELogin(driver);
		ll.WaitFunction();
		ll.Password().sendKeys("Venus2009!");
		ll.Login().click();
		LEDEHome lh = new LEDEHome(driver);
		lh.OpenWirelessNetworkOverview();
		lh.EnableDisableWirelessNetwork();
		lh.Logout();
	}

}
